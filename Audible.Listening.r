#Import Dataset:
getwd()
setwd("C:\\Users\\abdul\\Desktop\\R Programming")
Audible.Books <- read.csv(".\\Freestyle\\Audible.Listening.csv")

#Check out the Data:
str(Audible.Books)
head(Audible.Books)

#Convert Variable Modes as Necessary:
#Convert Dates to POSIX Date Format:
Audible.Books$�..Date <- as.POSIXct(Audible.Books$�..Date, format="%Y-%m-%d %H:%M:%S")
#Convert Titles to Factors:
Audible.Books$Title <- factor(Audible.Books$Title)

#Make a Simpler Dataframe with Nicer Variable Names:
Audible.Books <- data.frame(Date=Audible.Books$�..Date, Book=Audible.Books$Title)

#Change Date Variable to Year Only, Then Turn it Into a Factor Variable:
Audible.Books$Date <- format(Audible.Books$Date, format = "%Y")
Audible.Books$Date <- factor(Audible.Books$Date)

#Final Check of the Data:
head(Audible.Books)
str(Audible.Books)
summary(Audible.Books)

#Import ggplot2:
library(ggplot2)

#Make the Histogram:
ggplot(data = Audible.Books, aes(x=Date)) +
  geom_histogram(stat = "count", fill="blue", binwidth = 2, bins = 2) +
  labs(title = "Audible Listening Sessions per Year") +
  xlab("Year") + ylab("Sessions") +
  theme(plot.title = element_text(hjust = 0.5), axis.text = element_text(colour = "red"))
  

#It looks ugly! Much code for little result...